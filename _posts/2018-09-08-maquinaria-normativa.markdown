---
layout: default
intro: true
title: "Maquinaria normativa"
date: 2018-09-08 19:18:17 -0300
tag: posts
---

# Maquinaria normativa

Publicar constantemente nuestra vida privada en internet?
por qué/como pasó esto? como se hizo *lo comun*?

Son empresas. Trabajamos para ellas. En nuestro tiempo libre.
Son empresas. Normatizan nuestro cotidiano, nuestros vinculos.
Som empresas. Y no tenemos soberania sobre nuestros datos.

Solo alimentamos la maquinaria
(y nuestro "yo", claro. Hola Narciso soy Ego)

Y también publicamos en las redes a-sociales nuestras actividades re-creativas?
Es la unión perfectamente lograda de creatividad + capital.

"Sirven para difundir" ???. Si necesitamos y usamos eso para difundir
nuestros proyectos artisticos, que estamos realmente difundiendo?
que estamos haciendo? Algo consumible.
