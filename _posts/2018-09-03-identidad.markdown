---
layout: default
intro: true
title: "Identidad?"
date: 2018-09-06 13:53:17 -0300
tag: posts
---

## Identidad? o mejor proceso de identificación?

Varixs autorxs ya lo dijeron (ahora recuerdo a Bifo), que la identidad no es estática.
Encontes que mute, que se mueva: que sea un proceso dinámico.

Al pensar mi identidad, me doy el lugar a que sea temporal, que sea un proceso continuio,
un gran "%s/identidad/proceso de identificacion/g"

Ahora soy esta. En un rato soy esta otra.
O hasta puedo ser muchas cosas a la vez! y matchear 1:1 con las identidades digitales en internet.

